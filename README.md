Open Dispatch
=============

*Open Dispatch* is a micro-service daemon written in Go along with a single-page
Web UI in Vue.js which handles dispatching orders from multiple parties to
multiple parties in an local urban delivery setting. 

Documentation to install or develop Open Dispatch is here::

- [Installing](docs/installing.md)
- [API](docs/api.md)
- [Developing](docs/developing.md)
- [Web UI](docs/web-ui.md)

*Open Dispatch* has been built around a homebrew experimental cryptographic identity/verification system called 
[Simple Signed Requests](https://codeberg.org/eotl/simple-signed-records)
instead of normal email/password login. Here be dragons :dragon: this is
unmaintained since awhile.... but stay tuned :smirk:*
