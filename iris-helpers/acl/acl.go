package acl

import (
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/ssr"
	"reflect"
)

type ACLChecker func(identity ssr.Identity, _type *reflect.Type, object interface{}, action string) bool

type ACL struct {
	sealed    bool
	allowed   []ACLChecker
	forbidden []ACLChecker
}

func (acl *ACL) AllowByPattern(entryRoles []string, entryIdentity ssr.Identity, entryTypes []reflect.Type, entryActions []string) {
	if acl.sealed {
		panic("Tried to change sealed ACL")
	}
	acl.allowed = append(acl.allowed, matches(entryRoles, entryIdentity, entryTypes, entryActions))
}

func (acl *ACL) AllowByFunction(checker ACLChecker) {
	if acl.sealed {
		panic("Tried to change sealed ACL")
	}
	acl.allowed = append(acl.allowed, checker)
}

func (acl *ACL) ForbidByPattern(entryRoles []string, entryIdentity ssr.Identity, entryTypes []reflect.Type, entryActions []string) {
	if acl.sealed {
		panic("Tried to change sealed ACL")
	}
	acl.forbidden = append(acl.forbidden, matches(entryRoles, entryIdentity, entryTypes, entryActions))
}

func (acl *ACL) ForbidByFunction(checker ACLChecker) {
	if acl.sealed {
		panic("Tried to change sealed ACL")
	}
	acl.forbidden = append(acl.forbidden, checker)
}

func (acl *ACL) Seal() {
	acl.sealed = true
}

func matches(entryRoles []string, entryIdentity ssr.Identity, entryTypes []reflect.Type, entryActions []string) func(identity ssr.Identity, _type *reflect.Type, object interface{}, action string) bool {
	return func(identity ssr.Identity, _type *reflect.Type, object interface{}, action string) bool {

		roleResult := false
		if entryRoles == nil {
			roleResult = true
		} else {
			for _, identityRole := range identity.Roles() {
				for _, entryRole := range entryRoles {
					if entryRole == identityRole {
						roleResult = true
						break
					}
				}
				if roleResult {
					break
				}
			}
		}

		identityResult := entryIdentity == nil || entryIdentity.Id() == identity.Id()

		typeResult := false
		if entryTypes == nil {
			typeResult = true
		} else {
			for _, entryType := range entryTypes {
				if entryType == *_type {
					typeResult = true
					break
				}
			}
		}

		actionResult := false
		if entryActions == nil {
			actionResult = true
		} else {
			for _, entryAction := range entryActions {
				if entryAction == "__read__" && (action == "read" || action == "list") {
					actionResult = true
					break
				}
				if entryAction == action {
					actionResult = true
					break
				}
			}
		}

		return roleResult && identityResult && actionResult && typeResult
	}
}

func (acl *ACL) Can(identity ssr.Identity, _type *reflect.Type, object interface{}, action string) bool {
	allowed := false

	for _, entry := range acl.allowed {
		if entry(identity, _type, object, action) {
			allowed = true
			break
		}
	}

	for _, entry := range acl.forbidden {
		if entry(identity, _type, object, action) {
			allowed = false
			break
		}
	}

	return allowed
}

func New() *ACL {
	return &ACL{
		false,
		[]ACLChecker{},
		[]ACLChecker{},
	}
}
