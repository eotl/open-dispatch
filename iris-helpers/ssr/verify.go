package ssr

import (
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers"
	"codeberg.org/eotl/open-dispatch-daemon/utils/encoding/bech32"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
	"net/http"
	"strings"

	"codeberg.org/eotl/open-dispatch-daemon/crypto/cert"
	"codeberg.org/eotl/open-dispatch-daemon/crypto/eddsa"
)

const signDataKey string = "sign-data"
const originalRecordKey = "original-record"
const certifiedBodyKey string = "certified-body"
const identityKey = "certified-identity"

type IdentityProvider func(id string) (Identity, error)

type Identity interface {
	Id() string
	Roles() []string
}

type SignData struct {
	Identity  string `json:"identity"`
	ID        string `json:"id"`
	Signature string `json:"signature"`
}

// NewVerifyRequest creates a middleware for verifying signed requests.
func RequestVerification(provider IdentityProvider, exclusionPaths ...string) context.Handler {
	return func(ctx iris.Context) {
		for _, exclusionPath := range exclusionPaths {
			if ctx.Path() == exclusionPath {
				ctx.Next()
				return
			}
		}
		if method := ctx.Method(); method == "POST" || method == "PUT" {
			verifyBodyRequest(ctx, provider)
		} else {
			verifyNoBodyRequest(ctx, provider)
		}
		return
	}
}

func verifyBodyRequest(ctx iris.Context, provider IdentityProvider) {
	authenticationBytes, err := ctx.GetBody()
	if err != nil {
		iris_helpers.AbortWithError(ctx, http.StatusBadRequest,
			fmt.Errorf("Could not read request body: %s", err))
		return
	}

	signData, certified, err := ExtractSignData(authenticationBytes)
	if err != nil {
		iris_helpers.AbortWithError(ctx, http.StatusBadRequest,
			err)
		return
	}

	ctx.Values().Set(certifiedBodyKey, certified)
	ctx.Values().Set(originalRecordKey, authenticationBytes)
	ctx.Values().Set(signDataKey, *signData)

	identity, err := provider(signData.Identity)
	if err != nil {
		iris_helpers.AbortWithError(ctx, http.StatusInternalServerError,
			err)
		return
	}
	ctx.Values().Set(identityKey, identity)
	ctx.Next()
}

func verifyNoBodyRequest(ctx iris.Context, provider IdentityProvider) {
	authenticationHeaderString := ctx.GetHeader("Authorization")
	var authenticationString string
	if !strings.HasPrefix(authenticationHeaderString, "SSR ") {
		authenticationParamString := ctx.URLParam("certificate")
		if len(authenticationParamString) == 0 {
			iris_helpers.AbortWithError(ctx, http.StatusUnauthorized,
				fmt.Errorf("No authentication found"))
			return
		} else {
			authenticationString = authenticationParamString
		}
	} else {
		authenticationString = authenticationHeaderString[4:]
	}

	authenticationBytes, err := base64.URLEncoding.DecodeString(authenticationString)
	if err != nil {
		iris_helpers.AbortWithError(ctx, http.StatusBadRequest,
			fmt.Errorf("Header could not be decoded: %s", err))
		return
	}

	signData, _, err := ExtractSignData(authenticationBytes)
	if err != nil {
		iris_helpers.AbortWithError(ctx, http.StatusBadRequest,
			err)
		return
	}

	ctx.Values().Set(signDataKey, *signData)
	identity, err := provider(signData.Identity)
	if err != nil {
		iris_helpers.AbortWithError(ctx, http.StatusInternalServerError,
			err)
		return
	}
	ctx.Values().Set(identityKey, identity)
	ctx.Next()
}

func ExtractSignData(authenticationBytes []byte) (*SignData, []byte, error) {
	signatures, err := cert.GetSignatures(authenticationBytes)
	if err != nil {
		return nil, nil, fmt.Errorf("Could not get signatures: %s", err)
	}

	if len(signatures) != 1 {
		return nil, nil, fmt.Errorf("There must be exactly one signature")
	}

	sig := signatures[0]

	pubKey := new(eddsa.PublicKey)
	decoded, err := bech32.DecodeBech32ToBytes(sig.Identity)
	if err != nil {
		return nil, nil, err
	}
	err = pubKey.FromBytes(decoded)

	if err != nil {
		return nil, nil, fmt.Errorf("Malformed Identity Key: %s", err)
	}

	certified, err := cert.Verify(pubKey, authenticationBytes)
	if err != nil {
		return nil, nil, fmt.Errorf("Request could not be verified: %s", err)
	}

	// XXX:  TODO: check that the key is whitelisted

	sum := sha256.Sum256(authenticationBytes)
	requestID, err := bech32.ConvertBytesToBech32(sum[:])
	if err != nil {
		return nil, nil, err
	}

	signData := SignData{
		Identity:  pubKey.String(),
		Signature: base64.URLEncoding.EncodeToString(sig.Payload),
		ID:        requestID,
	}
	return &signData, certified, nil
}

func GetSignData(ctx iris.Context) SignData {
	data, ok := ctx.Values().Get(signDataKey).(SignData)
	if !ok {
		panic("No signed data in the request")
	}
	return data
}

func GetCertifiedBody(ctx iris.Context) []byte {
	data, ok := ctx.Values().Get(certifiedBodyKey).([]byte)
	if !ok {
		panic("No signed data in the request")
	}
	return data
}

func GetOriginalRecord(ctx iris.Context) []byte {
	data, ok := ctx.Values().Get(originalRecordKey).([]byte)
	if !ok {
		panic("No signed data in the request")
	}
	return data
}

func GetIdentity(ctx iris.Context) Identity {
	data, ok := ctx.Values().Get(identityKey).(Identity)
	if !ok {
		panic("No Identity Key in the request")
	}
	return data
}
