package model

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"time"
)

type Base struct {
	ID        uuid.UUID      `json:"id" gorm:"type:uuid;primarykey"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at" gorm:"index"`
}

func (base *Base) BeforeCreate(db *gorm.DB) error {
	newUuid, err := uuid.NewRandom()
	base.ID = newUuid
	return err
}

func (base Base) Id() string {
	return base.ID.String()
}

func (base Base) SetId(id string) {
	base.ID = uuid.MustParse(id)
}
