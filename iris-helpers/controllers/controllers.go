package controllers

import (
	irishelpers "codeberg.org/eotl/open-dispatch-daemon/iris-helpers"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/acl"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/ssr"
	"fmt"
	"github.com/google/uuid"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
	"gorm.io/gorm"
	"log"
	"net/http"
	"reflect"
	"strings"
)

type ControllerObject interface {
	Id() string
}

type ParseBody func(context iris.Context, db *gorm.DB) (ControllerObject, error)
type DBScope func(context iris.Context) *gorm.DB
type Converter func(idString string) interface{}
type Hook func(context iris.Context, object interface{})

type AdditionalHandlers struct {
	PathParameters map[string]string
	MethodHandlers map[string]AdditionalHandler
}

type AdditionalHandler struct {
	Handler    context.Handler
	InputType  reflect.Type
	OutputType reflect.Type
}

func StringConverter(idString string) interface{} {
	return idString
}

type RESTController struct {
	App                        *iris.Application
	ACL                        *acl.ACL
	DB                         *gorm.DB
	ControllerType             *reflect.Type
	PathPrefix                 string
	ParseBody                  ParseBody
	CustomCreateHandler        context.Handler
	CustomListHandler          context.Handler
	CustomReadHandler          context.Handler
	CustomUpdateHandler        context.Handler
	CustomDeleteHandler        context.Handler
	AdditionalHandlers         map[string]AdditionalHandlers
	AdditionalInstanceHandlers map[string]AdditionalHandlers
	SubControllers             map[string]RESTController
	Scope                      DBScope
	IdConverter                Converter
	OnCreate                   Hook
	OnUpdate                   Hook
	OnDelete                   Hook
}

func (controller RESTController) Mount(party iris.Party) {
	party.Post("/", controller.Create())
	party.Get("/", controller.List())
	party.Get("/"+controller.IdPath(), controller.Read())
	party.Put("/"+controller.IdPath(), controller.Update())
	party.Delete("/"+controller.IdPath(), controller.Delete())

	for path, additionalHandlers := range controller.AdditionalHandlers {
		for method, additionalHandler := range additionalHandlers.MethodHandlers {
			party.Handle(strings.ToUpper(method), "/"+path, additionalHandler.Handler)
		}
	}
	for path, additionalHandlers := range controller.AdditionalInstanceHandlers {
		for method, additionalHandler := range additionalHandlers.MethodHandlers {
			party.Handle(strings.ToUpper(method), "/"+controller.IdPath()+"/"+path, additionalHandler.Handler)
		}
	}

	for path, subController := range controller.SubControllers {
		subParty := party.Party("/" + controller.IdPath() + "/" + path)
		{
			subController.Mount(subParty)
		}
	}

	return
}

func (controller *RESTController) IdParamName() string {
	controllerType := *controller.ControllerType
	return IdParamName(controllerType)
}

func IdParamName(controllerType reflect.Type) string {
	return irishelpers.CamelToSnakeCase(controllerType.Name()) + "_id"
}

func (controller *RESTController) IdPath() string {
	return "{" + controller.IdParamName() + "}"
}

func (controller *RESTController) makeScope(ctx iris.Context) *gorm.DB {
	if controller.Scope == nil {
		return controller.DB
	}
	return controller.Scope(ctx)
}

func (controller *RESTController) Veto(ctx iris.Context, identity ssr.Identity, action string) {
	irishelpers.AbortWithError(
		ctx,
		http.StatusForbidden,
		fmt.Errorf(
			"key %s is not allowed to %s %s",
			identity.Id(),
			action,
			(*controller.ControllerType).Name()))
}

func (controller *RESTController) CanCreate(identity ssr.Identity, object interface{}) bool {
	return controller.ACL.Can(identity, controller.ControllerType, object, "create")
}
func (controller *RESTController) CanRead(identity ssr.Identity, object interface{}) bool {
	return controller.ACL.Can(identity, controller.ControllerType, object, "read")
}
func (controller *RESTController) CanUpdate(identity ssr.Identity, object interface{}) bool {
	return controller.ACL.Can(identity, controller.ControllerType, object, "update")
}
func (controller *RESTController) CanDelete(identity ssr.Identity, object interface{}) bool {
	return controller.ACL.Can(identity, controller.ControllerType, object, "delete")
}

func DefaultCreateHandler(controller *RESTController) context.Handler {
	return func(ctx iris.Context) {

		object, err := controller.ParseBody(ctx, controller.DB)
		if err != nil {
			log.Printf("Failed to construct %v from request: %v\n", controller.ControllerType, err)
			irishelpers.AbortWithError(ctx, http.StatusBadRequest, err)
			return
		}

		identity := ssr.GetIdentity(ctx)

		if controller.CanCreate(identity, object) {
			result := controller.DB.Create(object)
			ctx.StatusCode(http.StatusCreated)
			EncodeAndReturn(ctx, result, object)
		} else {
			controller.Veto(ctx, identity, "create")
			return
		}
		if controller.OnCreate != nil {
			controller.OnCreate(ctx, object)
		}
	}
}

func (controller *RESTController) Create() context.Handler {
	if controller.CustomCreateHandler != nil {
		return controller.CustomCreateHandler
	} else {

		return DefaultCreateHandler(controller)
	}
}

func (controller *RESTController) List() context.Handler {
	if controller.CustomListHandler != nil {
		return controller.CustomListHandler
	} else {
		return func(ctx iris.Context) {
			identity := ssr.GetIdentity(ctx)
			if controller.CanRead(identity, nil) {
				slice := reflect.New(reflect.SliceOf(*controller.ControllerType)).Interface()
				result := controller.makeScope(ctx).Find(slice)
				EncodeAndReturn(ctx, result, slice)
				return
			} else {
				controller.Veto(ctx, identity, "list")
				return
			}
		}
	}
}

func (controller *RESTController) Read() context.Handler {
	if controller.CustomReadHandler != nil {
		return controller.CustomReadHandler
	} else {
		return func(ctx iris.Context) {
			object := reflect.New(*controller.ControllerType).Interface()
			id := controller.GetId(ctx)
			var result *gorm.DB

			switch id.(type) {
			case string:
				result = controller.makeScope(ctx).First(object, "id = ?", id)
			default:
				result = controller.makeScope(ctx).First(object, id)
			}

			identity := ssr.GetIdentity(ctx)
			if controller.CanRead(identity, object) {
				EncodeAndReturn(ctx, result, object)
			} else {
				controller.Veto(ctx, identity, "read")
			}
			return
		}
	}
}

func (controller *RESTController) Update() context.Handler {
	if controller.CustomUpdateHandler != nil {
		return controller.CustomUpdateHandler
	} else {
		return func(ctx *context.Context) {
			object, err := controller.ParseBody(ctx, controller.DB)
			if err != nil {
				irishelpers.AbortWithError(ctx, http.StatusBadRequest, fmt.Errorf("Invalid %v from request: %v\n", controller.ControllerType, err))
				return
			}
			identity := ssr.GetIdentity(ctx)
			if controller.CanUpdate(identity, object) {
				result := controller.DB.Save(object)
				EncodeAndReturn(ctx, result, object)
			} else {
				controller.Veto(ctx, identity, "update")

			}
			if controller.OnUpdate != nil {
				controller.OnUpdate(ctx, object)
			}
			return
		}
	}
}

func (controller *RESTController) Delete() context.Handler {
	if controller.CustomDeleteHandler != nil {
		return controller.CustomDeleteHandler
	} else {
		return func(ctx iris.Context) {
			object := reflect.New(*controller.ControllerType).Interface()

			result := controller.DB.First(object, "id = ?", controller.GetId(ctx))
			identity := ssr.GetIdentity(ctx)
			if controller.CanDelete(identity, object) {
				EncodeAndReturn(ctx, result, object)
				if controller.OnDelete != nil {
					controller.OnDelete(ctx, object)
				}
			} else {
				controller.Veto(ctx, identity, "delete")
			}

			return
		}
	}
}

func (controller *RESTController) GetId(ctx iris.Context) interface{} {

	idString := controller.GetIdString(ctx)
	if controller.IdConverter != nil {
		return controller.IdConverter(idString)
	} else {
		return uuid.MustParse(idString)
	}

}

func GetIdString(ctx iris.Context, controllerType reflect.Type) string {

	return ctx.Params().Get(IdParamName(controllerType))

}

func (controller *RESTController) GetIdString(ctx iris.Context) string {
	return ctx.Params().Get(controller.IdParamName())
}

func EncodeAndReturn(ctx iris.Context, dbResult *gorm.DB, returnObject interface{}) {
	if dbResult.Error != nil {
		log.Printf("Failed database operation: %v\n", dbResult.Error)
		irishelpers.AbortWithError(ctx, http.StatusInternalServerError, fmt.Errorf("database error"))
	} else {
		ctx.Header("Content-Type", "application/json")
		_, _ = ctx.JSON(returnObject)
	}
}
