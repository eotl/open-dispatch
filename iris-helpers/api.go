package iris_helpers

import (
	"encoding/json"
	"fmt"
	"github.com/kataras/iris/v12"
	"log"
	"net/http"
	"strconv"
)

// AbortWithError abort a request and writes an api error.
func AbortWithError(ctx iris.Context, code int, err error) {
	log.Printf("Failed request: %v\n", err)
	ctx.Header("content-type", "application/json")
	ctx.StatusCode(code)
	writeErr := json.NewEncoder(ctx.ResponseWriter()).Encode(&ApiError{
		Error:       strconv.Itoa(code),
		Description: err.Error(),
	})
	if writeErr != nil {
		fmt.Println("could not write response") // TODO: loglevel debug
	}
}

type ApiError struct {
	Error       string `json:"error"`
	Description string `json:"description"`
}

func HandleInternalError(ctx iris.Context, app *iris.Application, err error) {
	app.Logger().Error(err)
	AbortWithError(ctx, http.StatusInternalServerError, err)
}
