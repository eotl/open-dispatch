package utils

import (
	"reflect"
)

type M = map[string]interface{}
type A = []interface{}

func UpdateMap(original, update M) M {
	for key, updateVal := range update {
		if originalVal, present := original[key]; present {
			if isMap(originalVal) && isMap(updateVal) {
				original[key] = UpdateMap(originalVal.(M), updateVal.(M))
			} else if isArray(originalVal) && isArray(updateVal) {
				original[key] = UpdateArray(originalVal.(A), updateVal.(A))
			} else {
				if reflect.TypeOf(originalVal) == reflect.TypeOf(updateVal) {
					original[key] = updateVal
				}
			}
		} else {
			// key not in left so we can just shove it in
			original[key] = updateVal
		}
	}
	return original
}

func UpdateArray(original, update A) A {
	for key, updateVal := range update {
		if len(original) >= key+1 {
			originalVal := original[key]
			if isMap(originalVal) && isMap(updateVal) {
				original[key] = UpdateMap(originalVal.(M), updateVal.(M))
			} else if isArray(originalVal) && isArray(updateVal) {
				original[key] = UpdateArray(originalVal.(A), updateVal.(A))
			} else {
				if reflect.TypeOf(originalVal) == reflect.TypeOf(updateVal) {
					original[key] = updateVal
				}
			}
		} else {
			// key not in left so we can append it
			original = append(original, updateVal)
		}
	}
	return original

}

func isMap(toCheck interface{}) bool {
	_, ok := toCheck.(M)
	return ok
}

func isArray(toCheck interface{}) bool {
	_, ok := toCheck.(A)
	return ok
}
