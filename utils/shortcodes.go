package utils

import (
	"github.com/matoous/go-nanoid/v2"
)

const SHORTCODE_ALPHABET = "ACDEFGHJKLMNPQRTUVWXY34679"

func ShortCode() string {
	return gonanoid.MustGenerate(SHORTCODE_ALPHABET, 3)
}
