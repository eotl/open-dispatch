package main

import (
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/ssr"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	data := []byte(stdin())
	_, certified, err := ssr.ExtractSignData(data)
	if err != nil {
		log.Fatalln("could not verify ", err)
	}

	print(string(certified))

}

func stdin() string {
	fi, err := os.Stdin.Stat()
	if err != nil {
		return ""
	}
	if fi.Mode()&os.ModeNamedPipe == 0 && !fi.Mode().IsRegular() {
		return ""
	}

	bytes, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		return ""
	}
	return string(bytes)
}
