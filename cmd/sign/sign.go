package main

import (
	"crypto/ed25519"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"codeberg.org/eotl/open-dispatch-daemon/crypto/cert"
	"codeberg.org/eotl/open-dispatch-daemon/crypto/eddsa"
	"golang.org/x/crypto/ssh"
)

func main() {
	privKeyPath := flag.String("p", "privkey", "Private key file, must be ed25519")
	flag.Parse()
	var privKey eddsa.PrivateKey
	privKey.FromBytes(*parseSSHPrivate(*privKeyPath))
	data := []byte(stdin())
	signed, err := cert.Sign(&privKey, data, time.Now().Add(time.Hour).Unix())
	if err != nil {
		log.Fatalln("could not sign ", err)
	}
	fmt.Println(string(signed))
}

func parseSSHPrivate(path string) *ed25519.PrivateKey {
	handle, err := os.Open(path)
	if err != nil {
		log.Fatalln("could not open "+path, err)
	}
	defer handle.Close()
	bytes, err := ioutil.ReadAll(handle)
	if err != nil {
		log.Fatalln("could not read "+path, err)
	}
	key, err := ssh.ParseRawPrivateKey(bytes)
	if err != nil {
		log.Fatalln("could not parse "+path, err)
	}
	return key.(*ed25519.PrivateKey)
}

func stdin() string {
	fi, err := os.Stdin.Stat()
	if err != nil {
		return ""
	}
	if fi.Mode()&os.ModeNamedPipe == 0 && !fi.Mode().IsRegular() {
		return ""
	}

	bytes, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		return ""
	}
	return string(bytes)
}
