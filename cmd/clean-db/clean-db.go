package main

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
)

func main() {
	var dbUrl string
	if env, present := os.LookupEnv("DB_URL"); present {
		dbUrl = env
	} else {
		dbUrl = "postgres://dispatch:skrtpsswd@127.0.0.1:5432/dispatch?sslmode=disable"
	}
	db, err := gorm.Open(postgres.Open(dbUrl), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	db.Exec("DROP SCHEMA public CASCADE;")
	db.Exec("CREATE SCHEMA public;")
	db.Exec("GRANT ALL ON SCHEMA public TO dispatch;")
	db.Exec("COMMENT ON SCHEMA public IS 'standard public schema';")
}
