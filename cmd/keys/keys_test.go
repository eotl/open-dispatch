package main

import (
	"crypto/ed25519"
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"codeberg.org/eotl/open-dispatch-daemon/crypto/eddsa"
	"golang.org/x/crypto/ssh"
)

// Returns eddsa public key String()
// by parsing ssh private key
func ParseSSHPrivateKey(path string) (string, error) {
	var eddsaPrivKey eddsa.PrivateKey
	var eddsaPubKey eddsa.PublicKey

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	sshPrivKey, err := ssh.ParseRawPrivateKey(data)
	if err != nil {
		return "", err
	}

	privKeyBytes := sshPrivKey.(*ed25519.PrivateKey)

	eddsaPrivKey.FromBytes(*privKeyBytes)
	eddsaPubKey.FromBytes(eddsaPrivKey.PublicKey().Bytes())

	return eddsaPubKey.String(), nil
}

func TestGetSSHPubFileString(t *testing.T) {
	assert := assert.New(t)

	// With valid public key file
	expected, err := ParseSSHPrivateKey("../../testkey.ed25519.key")
	require.NoError(t, err, "ParseSSHPrivateKey() Test Helper")

	actual, err := GetSSHPubFileString("../../testkey.ed25519.key.pub")
	require.NoError(t, err, "GetSSHPubFileString()")
	assert.Equal(expected, actual, "GetSSHPubFileString()")

	// With private key file
	actual, err = GetSSHPubFileString("../../testkey.ed25519.key")
	require.Error(t, err, "GetSSHPubFileString()")
}
