// Copyright (C) 2020 Masala.
// main.go - open-dispatch server binary.
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"codeberg.org/eotl/open-dispatch-daemon/config"
	"codeberg.org/eotl/open-dispatch-daemon/listener"
	"context"
	"flag"
	"fmt"
	"github.com/kataras/iris/v12"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {

	port := flag.Int("p", 8000, "Port to listen on")
	configFile := flag.String("c", "./config.yaml", "Configuration File Path")
	flag.Parse()
	configuration := config.Initialize(*configFile)

	halt := make(chan os.Signal)
	signal.Notify(halt, os.Interrupt, syscall.SIGTERM)

	db, err := gorm.Open(postgres.Open(configuration.DBUrl), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	app, err := listener.New(db, configuration)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to start listener: %v\n", err)
		os.Exit(-1)
	}

	idleConnsClosed := make(chan struct{})
	iris.RegisterOnInterrupt(func() {
		timeout := 10 * time.Second
		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()
		// close all hosts.
		app.Shutdown(ctx)
		close(idleConnsClosed)
	})
	app.Listen(fmt.Sprintf(":%d", *port), iris.WithoutInterruptHandler, iris.WithoutServerError(iris.ErrServerClosed))
	<-idleConnsClosed
}
