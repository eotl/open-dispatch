---
title: "Rest API"
draft: false
lang: "English"
leafs: ["rest-api", "json"]
---

Rough outline of an API served by this daemon and what happens in the daemon

### POST /key_whitelist

Add new pubkey to whitelist

```
pubkey
```

### POST /key_blacklist

```
  pubkey
  admin_signature
```

- Verifies signature of adminkey
- Removes pubkey from whitelist (if exists)
- Adds pubkey to blacklist

### POST /create

Creates a new order if signed by whitelisted key or checks out with bloom filter

```
{
  "payload": {
  "uuid": "happy-star-whiskey-zulu",
  "note": "Hello my name is Fred. I need this delivered really urgently",
  "item_size": "medium",
  "item_weight": "heavy",
  "delivery_speed": "immediate",
  "to_address": "Some Strasse 123",
  "to_district": "Berlin",
  "parent_record": "13313adad918238913afbe6754..."
  },
  "signature": "az54535sgfd5623413...."
}
```

- Submission received
- Checks if key is whitelisted 
- Submission verified
- Submission references another submission by same pubkey y/n


### GET /info/{order_id}

- Fetch order specific data

### POST /update/{order_id}

- Checks if key is whitelisted 
- Check status of delivery

`created` - new delivery request

`updated` - delivery request (size, destination, etc.. might change)

`transit` - delivery being moved

`removed` - delivery request was canceled by sender

`completed` - delivery was dropped off

Marks delivery as `completed`
Sends new notification (if change occurs)

### GET /delete/{order_id}

Deletes an order from the system

- `Y` removes order record
- `N` does nothing

