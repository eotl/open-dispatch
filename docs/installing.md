---
title: "Installing"
draft: false
lang: "English"
leafs: []
---

You must have Go development environment working. Please follow the official
[install instructions](https://golang.org/doc/install) to get going. Once done,
proceed with the following

### Postgres (docker)

Use Docker to start a postgres database:
```
$ docker-compose up -d
```

### Postgres (manual)

For install on Debian 10+ machines

```
$ sudo apt install postgresql
```

Create a database and user with credentials to create databases

```
$ sudo -u postgres psql
[sudo] password for user:
psql (12.2 (Debian 12.2-1+b1))
Type "help" for help.

postgres=# CREATE USER dispatch WITH ENCRYPTED PASSWORD 'skrtpsswrd' CREATEDB;
postgres=# CREATE DATABASE dispatch OWNER dispatch;
postgres=# GRANT ALL PRIVILEGES ON DATABASE dispatch to dispatch;
postgres=# \c dispatch
dispatch=# ALTER SCHEMA public OWNER TO dispatch;
dispatch=# \q
```


### Clone repo and create config.yaml

```
$ git clone https://codeberg.org/eotl/open-dispatch-daemon
$ cd open-dispatch-daemon
```

Copy the example `.yaml` file to where you plan to run the `server` from
and edit the values contained therein to match your instance.

```
$ cp config.example.yaml config.yaml
```

## Build Daemon

```
$ cd cmd/server
$ go build
$ cd -
```

Assuming it builds with no errors, you should be able to the run the daemon 


## Build Web UI

For now follow instructions in the `www/README.md` file.


## Running

Ensure your postgres db daemon is running e.g.

```
# systemctl start postgresql.service
```

If your database needs a different access url add the following ENV variable.
Needed for any _command_ that interacts with the database.

```
$ export DB_URL=postgres://dispatch:skrtpsswd@127.0.0.1:5432/dispatch?sslmode=disable 
```

On the first run of the server it will create the dB tables.
You can run it on a custom port with the `-p port` flag (default: 8000).

```
$ ./cmd/server/server
```
