---
title: "Developing"
draft: false
lang: "English"
leafs: []
---

## Clean Database

```
$ go run cmd/clean-db/clean-db.go
```

## API Documentation

Documentation and APIs are a work-in-progress ;)

- OpenAPI specification [http://localhost:8000/api/v1/spec](http://localhost:8000/api/v1/spec)
- API endpoints [http://localhost:8000/info](http://localhost:8000/info)


## Testing

Make sure the following tools/dependencies are installed: [httpie](https://httpie.io), jq, base64

Run our testing script

```
$ ./end-to-end-test.sh
```

Run basic testing coverage from the root project directory with

```
$ go test ./...
```


*Tested with minimum [Go](https://golang.org) version 1.13.6*


### Sign and Verify Requests

Use [sign.go](cmd/sign/sign.go) to sign a request.

This example creates a signed shipment request and posts it
to the server:

```
$ cat test/shipment_create.json | go run cmd/sign/sign.go -p testkey.ed25519.key | http POST ':8000/api/v1/shipments'
```

Use [verify.go](cmd/verify/verify.go) to verify signed requests

```
$ cat test/shipment_create.json | go run cmd/sign/sign.go -p testkey.ed25519.key | go run cmd/verify/verify.go 
```
