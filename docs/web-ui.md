---
title: "Web UI"
draft: false
lang: "English"
leafs: []
---

A configurable HTML5 / Vue.js web interface for *Open Dispatch*

## Install

You need [Yarn](https://yarnpkg.com) package manager installed.

```
$ cd open-dispatch/
$ yarn install
```

## Developing

To compile and hot-reloads (watch) for development

```
$ yarn serve
```

## Building

To compiles and minifies for production do

```
$ yarn build
```

The files live in `dist/`

## Lints And Fixes

```
$ yarn lint
```

## Custom Configuration

See [Configuration Reference](https://cli.vuejs.org/config/)
