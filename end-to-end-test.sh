#! /usr/bin/env bash

set -e # Exit when command fails

readonly DEPS="jq base64 http"

error() { printf "Error: ${1}\n"; exit 1; }

check_deps() {
	for c in $DEPS; do
		command -v "$c" > /dev/null || error "Command not found, please install it: ${c}"
	done
}

check_deps

mkdir -p logs
#TEST_LOG_FILE=logs/end2end-$(date -u +"%Y%m%d%H%M%SZ").log
TEST_LOG_FILE=logs/end2end.log
#SERVER_LOG_FILE=logs/server-$(date -u +"%Y%m%d%H%M%SZ").log
SERVER_LOG_FILE=logs/server.log

onexit(){

  echo "Killing server" | tee -a "$TEST_LOG_FILE"
  kill "${SERVER_PID}" >> "$TEST_LOG_FILE" 2>&1
  echo "... Done" | tee -a "$TEST_LOG_FILE"

  if [[ -z "${SUCCESS}" ]]; then
    echo ""
    echo ""
    echo "#######################################"
    echo "# Client log:"
    echo "#######################################"
    echo ""

    cat "$TEST_LOG_FILE"

    echo ""
    echo ""
    echo "#######################################"
    echo "# Server log:"
    echo "#######################################"
    echo ""

    cat "$SERVER_LOG_FILE"
  fi
}
trap onexit EXIT

base64_nowrap() {
	# some base64 implementaitons do not have a "no wrap" option
	# this wrapper implements it independently
	while read i
	do
		echo "$i"
	done | base64 | tr -d '\n\r'
}

echo "Running end2end test for open-dispatch-daemon. Logs are written to $TEST_LOG_FILE" | tee "$TEST_LOG_FILE"
mkdir -p logs

echo "Cleaning and recreating database ..." | tee -a "$TEST_LOG_FILE"
go run cmd/clean-db/clean-db.go
echo "... Done" | tee -a "$TEST_LOG_FILE"

echo "Building server ..." | tee -a "$TEST_LOG_FILE"
(cd cmd/server && go build) >> "$TEST_LOG_FILE" 2>&1
echo "... Done. " | tee -a "$TEST_LOG_FILE"

echo "Starting server ..." | tee -a "$TEST_LOG_FILE"
cmd/server/server > "$SERVER_LOG_FILE" 2>&1 & SERVER_PID=${!} # Storing server pid so we can kill it after script is done
echo "... Done. Server running with PID $SERVER_PID, logging output to $SERVER_LOG_FILE"


# Test 1
echo "Creating first shipment and implicitely creating first user that is automatically handed admin rights ..." | tee -a "$TEST_LOG_FILE"
SHIPMENT_CREATION_RESPONSE=$(cat test/shipment_create.json | go run cmd/sign/sign.go -p test/keys/testkey.ed25519.key | http --check-status -b POST ':8000/api/v1/shipments')
echo "$SHIPMENT_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
SHIPMENT_ID=$(echo "$SHIPMENT_CREATION_RESPONSE" | jq -r .id)
echo "... Done. First shipment created with id $SHIPMENT_ID" | tee -a "$TEST_LOG_FILE"


# Test 2
echo "Updating shipment ..." | tee -a "$TEST_LOG_FILE"
cat test/update_city.json | go run cmd/sign/sign.go -p test/keys/testkey.ed25519.key | http --check-status -b POST ":8000/api/v1/shipments/${SHIPMENT_ID}/updates" >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 3
echo "Retrieving shipment with authentication cert as header" | tee -a "$TEST_LOG_FILE"
ADMIN_AUTH_CERT=$(echo '{}' | go run cmd/sign/sign.go -p test/keys/testkey.ed25519.key | base64_nowrap)
ADMIN_AUTH_HEADER="Authorization:SSR $ADMIN_AUTH_CERT"
http --check-status GET ":8000/api/v1/shipments/${SHIPMENT_ID}" "$ADMIN_AUTH_HEADER" >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 4
echo "Retrieving shipment with authentication cert as url parameter" | tee -a "$TEST_LOG_FILE"
http --check-status GET ":8000/api/v1/shipments/${SHIPMENT_ID}?certificate=$ADMIN_AUTH_CERT" >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 5
echo "Retrieving shipment with authentication cert without authentication. Expected to fail." | tee -a "$TEST_LOG_FILE"
set +e
http --check-status GET ":8000/api/v1/shipments/${SHIPMENT_ID}" >> "$TEST_LOG_FILE" 2>&1
if [ ! "$?" == 4 ]; then
    exit 1 # Exit if command does not have an exit code == 4
fi
set -e
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 6
echo "Creating invite as admin user" | tee -a "$TEST_LOG_FILE"
INVITE_CREATION_RESPONSE=$(cat test/invite.json | go run cmd/sign/sign.go -p test/keys/testkey.ed25519.key | http --check-status -b POST ':8000/api/v1/invites')
echo "$INVITE_CREATION_RESPONSE" | jq . >> "$TEST_LOG_FILE"
INVITE_ID=$(echo "$INVITE_CREATION_RESPONSE" | jq -r .id)
echo "... Done. Invite created with id $INVITE_ID" | tee -a "$TEST_LOG_FILE"


# Test 7
echo "Retrieving invite as admin user" | tee -a "$TEST_LOG_FILE"
http --check-status GET ":8000/api/v1/invites/$INVITE_ID" "$ADMIN_AUTH_HEADER" >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 8 #FIXME create test that it doesn't work with test/invite-fake.json
echo "Using invite" | tee -a "$TEST_LOG_FILE"
cat test/invite.json | go run cmd/sign/sign.go -p test/keys/testkey2.ed25519.key | http --check-status -b POST ':8000/api/v1/invites/use' >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 9
echo "List keys as admin" | tee -a "$TEST_LOG_FILE"
http --check-status GET ":8000/api/v1/keys" "$ADMIN_AUTH_HEADER" >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Tet 10
echo "Adding data to key as admin" | tee -a "$TEST_LOG_FILE"
cat test/add_info_to_key.json | go run cmd/sign/sign.go -p test/keys/testkey.ed25519.key | http --check-status -b PUT ':8000/api/v1/keys/ssr15ak9aak7tryc4sqy7lpfz6myawt8ds84d7fu4cnmjhpeuza0h6xsqy24t8' >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 11
echo "Retrieving key as admin" | tee -a "$TEST_LOG_FILE"
http --check-status GET ":8000/api/v1/keys/ssr15ak9aak7tryc4sqy7lpfz6myawt8ds84d7fu4cnmjhpeuza0h6xsqy24t8" "$ADMIN_AUTH_HEADER" >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 12
echo "Creating group as admin user" | tee -a "$TEST_LOG_FILE"
GROUP_CREATION_RESPONSE=$(cat test/create_group.json | go run cmd/sign/sign.go -p test/keys/testkey.ed25519.key | http --check-status -b POST ':8000/api/v1/groups')
echo "$GROUP_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
GROUP_ID=$(echo "$GROUP_CREATION_RESPONSE" | jq -r .id)
echo "... Done. Group created with id $GROUP_ID" | tee -a "$TEST_LOG_FILE"


# Test 13
echo "Creating shipment as verified user" | tee -a "$TEST_LOG_FILE"
SHIPMENT_CREATION_RESPONSE=$(cat test/shipment_create.json | go run cmd/sign/sign.go -p test/keys/testkey2.ed25519.key | http --check-status -b POST ':8000/api/v1/shipments')
echo "$SHIPMENT_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
SHIPMENT_ID=$(echo "$SHIPMENT_CREATION_RESPONSE" | jq -r .id)
echo "... Done. Shipment created with id $SHIPMENT_ID" | tee -a "$TEST_LOG_FILE"


# Test 14
echo "Updating shipment as owner..." | tee -a "$TEST_LOG_FILE"
cat test/update_city.json | go run cmd/sign/sign.go -p test/keys/testkey2.ed25519.key | http --check-status -b POST ":8000/api/v1/shipments/${SHIPMENT_ID}/updates" >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 15
echo "Assigning role supplier to shipment by owner..." | tee -a "$TEST_LOG_FILE"
cat test/add_supplier_to_shipment.json | go run cmd/sign/sign.go -p test/keys/testkey2.ed25519.key | http --check-status -b POST ":8000/api/v1/shipments/${SHIPMENT_ID}/updates" >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 16
echo "Assigning role delivery to shipment by supplier..." | tee -a "$TEST_LOG_FILE"
cat test/add_deliverer_to_shipment.json | go run cmd/sign/sign.go -p test/keys/testkey3.ed25519.key | http --check-status -b POST ":8000/api/v1/shipments/${SHIPMENT_ID}/updates" >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 17
echo "Updating shipment as deliverer..." | tee -a "$TEST_LOG_FILE"
cat test/update_weight.json | go run cmd/sign/sign.go -p test/keys/testkey4.ed25519.key | http --check-status -b POST ":8000/api/v1/shipments/${SHIPMENT_ID}/updates" >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 18
echo "Trying to read shipment without role. Expected to fail..." | tee -a "$TEST_LOG_FILE"
set +e
AUTH_CERT=$(echo '{}' | go run cmd/sign/sign.go -p test/keys/testkey5.ed25519.key | base64_nowrap)
AUTH_HEADER="Authorization:SSR $AUTH_CERT"
http --check-status GET ":8000/api/v1/shipments/${SHIPMENT_ID}" "$AUTH_HEADER" >> "$TEST_LOG_FILE" 2>&1
if [ ! "$?" == 4 ]; then
    exit 1 # Exit if command does not have an exit code == 4
fi
set -e
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 19
echo "Adding group member as group organizer user" | tee -a "$TEST_LOG_FILE"
GROUP_MEMBER_CREATION_RESPONSE=$(cat test/create_group_member.json | go run cmd/sign/sign.go -p test/keys/testkey.ed25519.key | http --check-status -b POST ":8000/api/v1/groups/$GROUP_ID/members")
echo "$GROUP_MEMBER_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
GROUP_MEMBER_ID=$(echo "$GROUP_MEMBER_CREATION_RESPONSE" | jq -r .id)
echo "... Done. Group member created with id $GROUP_MEMBER_ID" | tee -a "$TEST_LOG_FILE"

# Test 20
echo "Adding short code as group organizer user" | tee -a "$TEST_LOG_FILE"
SHORTCODE_CREATION_RESPONSE=$(echo "{}" | go run cmd/sign/sign.go -p test/keys/testkey.ed25519.key | http --check-status -b POST ":8000/api/v1/groups/$GROUP_ID/short-codes")
echo "$SHORTCODE_CREATION_RESPONSE" >> "$TEST_LOG_FILE"
SHORTCODE_ID=$(echo "$SHORTCODE_CREATION_RESPONSE" | jq -r .id)
echo "... Done. Shortcode created with id $SHORTCODE_ID" | tee -a "$TEST_LOG_FILE"


# Test 21
echo "Listing short codes as group organizer user" | tee -a "$TEST_LOG_FILE"
ADMIN_AUTH_CERT=$(echo '{}' | go run cmd/sign/sign.go -p test/keys/testkey.ed25519.key | base64_nowrap)
ADMIN_AUTH_HEADER="Authorization:SSR $ADMIN_AUTH_CERT"
http --check-status GET ":8000/api/v1/groups/${GROUP_ID}/short-codes" "$ADMIN_AUTH_HEADER" >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 22
# FIXME check content of group info automatically
echo "Retrieving group info as group organizer user" | tee -a "$TEST_LOG_FILE"
ADMIN_AUTH_CERT=$(echo '{}' | go run cmd/sign/sign.go -p test/keys/testkey.ed25519.key | base64_nowrap)
ADMIN_AUTH_HEADER="Authorization:SSR $ADMIN_AUTH_CERT"
http --check-status GET ":8000/api/v1/groups/${GROUP_ID}" "$ADMIN_AUTH_HEADER" >> "$TEST_LOG_FILE" 2>&1
echo "... Done" | tee -a "$TEST_LOG_FILE"


# Test 23
echo "Assigning short code to shipment as deliverer" | tee -a "$TEST_LOG_FILE"
SHIPMENT_ASSIGNING_RESPONSE=$(echo "{\"short_code\": \"$SHORTCODE_ID\"}" | go run cmd/sign/sign.go -p test/keys/testkey4.ed25519.key | http --check-status -b POST ":8000/api/v1/shipments/$SHIPMENT_ID/assign-short-code")
echo "$SHIPMENT_ASSIGNING_RESPONSE" >> "$TEST_LOG_FILE"
ASSIGNMENT_ID=$(echo "$SHIPMENT_ASSIGNING_RESPONSE" | jq -r .id)
echo "... Done. Assignment created with id $ASSIGNMENT_ID" | tee -a "$TEST_LOG_FILE"


SUCCESS=true
