package config

import (
	"codeberg.org/eotl/open-dispatch-daemon/model"
	"fmt"
	"github.com/caarlos0/env/v6"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
)

type Config struct {
	Instance         string   `env:"INSTANCE"                            yaml:"instance"`
	Description      string   `env:"DESCRIPTION"                         yaml:"description"`
	Url              string   `env:"URL"                                 yaml:"url"`
	Languages        []string `env:"LANGUAGES" envSeparator:";"          yaml:"languages"`
	LanguageDefault  string   `env:"LANGUAGE_DEFAULT"                    yaml:"language_default"`
	Cities           []string `env:"CITIES" envSeparator:";"             yaml:"cities"`
	Countries        []string `env:"COUNTRIES" envSeparator:";"          yaml:"countries"`
	Notifications    []string `env:"NOTIFICATIONS" envSeparator:";"      yaml:"notifications"`
	CanMakeInvites   []string `env:"CAN_MAKE_INVITES" envSeparator:";"   yaml:"can_make_invites"`
	CanEditShipments []string `env:"CAN_EDIT_SHIPMENTS" envSeparator:";" yaml:"can_edit_shipments"`
	DBUrl            string   `env:"DB_URL"                              yaml:"db_url"`
	SMTPServer       string   `env:"SMTP_SERVER"                         yaml:"smtp_server"`
	SMTPPassword     string   `env:"SMTP_PASSWORD"                       yaml:"smtp_password"`
	SMTPUserName     string   `env:"SMTP_USERNAME"                       yaml:"smtp_username"`
	SMTPFromAddress  string   `env:"SMTP_FROM_ADDRESS"                   yaml:"smtp_from_address"`
	StaticFilesPath  string   `env:"STATIC_FILES_PATH"                   yaml:"static_files_path"`
}

func Initialize(path string) Config {
	configuration := Config{
		Instance:         "Open Dispatch",
		Languages:        []string{"en"},
		LanguageDefault:  "en",
		CanMakeInvites:   []string{model.LEVEL_ADMIN},
		CanEditShipments: []string{model.LEVEL_ADMIN},
		DBUrl:            "postgres://dispatch:skrtpsswd@127.0.0.1:5432/dispatch?sslmode=disable",
		StaticFilesPath:  "/dev/null",
	}
	handle, err := os.Open(path)
	if err != nil {
		log.Println("could not open "+path, err)
	}
	bytes, err := ioutil.ReadAll(handle)
	if err != nil {
		log.Println("could not read "+path, err)
	}
	if err := yaml.Unmarshal(bytes, &configuration); err != nil {
		log.Println(fmt.Sprintf("%+v\n", err))
	}
	if err := env.Parse(&configuration); err != nil {
		log.Println(fmt.Sprintf("%+v\n", err))
	}
	return configuration
}
