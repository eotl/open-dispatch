package notifications

import (
	"codeberg.org/eotl/open-dispatch-daemon/config"
	"codeberg.org/eotl/open-dispatch-daemon/model"
	"github.com/jordan-wright/email"
	"github.com/thoas/go-funk"
	"net/smtp"
)

type Notification struct {
	Subject string
	Body    string
}

func NotifyKey(key model.IdentityKey, notification Notification, config config.Config) {
	if recipient, present := key.Info["email"]; present && funk.ContainsString(config.Notifications, "email") {
		SendEmail(recipient.(string), notification, config)
	}
}

func SendEmail(recipient string, notification Notification, config config.Config) {
	e := email.NewEmail()
	e.From = config.SMTPFromAddress
	e.To = []string{recipient}
	e.Subject = notification.Subject
	e.Text = []byte(notification.Body)
	if err := e.Send(
		config.SMTPServer,
		smtp.PlainAuth("", config.SMTPUserName, config.SMTPPassword, config.SMTPServer),
	); err != nil {

	}
}
