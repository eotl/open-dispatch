package test

import (
	"codeberg.org/eotl/open-dispatch-daemon/crypto/eddsa"
)

var PrivateKey *eddsa.PrivateKey

func init() {
	var err error
	PrivateKey, err = eddsa.NewKeypair(&NullReader{})
	if err != nil {
		panic(err)
	}
}
