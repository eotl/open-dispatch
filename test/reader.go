package test

import (
	"errors"
	"io"
)

type NullReader struct {
	io.Reader
}

func (*NullReader) Read(p []byte) (n int, err error) {
	return len(p), nil
}

type ErrReader struct {
	io.Reader
}

func (*ErrReader) Read(p []byte) (n int, err error) {
	return 0, errors.New("whoops")
}
