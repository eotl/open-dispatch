package main

import (
	"codeberg.org/eotl/open-dispatch-daemon/utils"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	originalPath := flag.String("o", "original-record", "json file containing original record")
	updatePath := flag.String("u", "update", "json file containing update")
	flag.Parse()

	handle, err := os.Open(*originalPath)
	if err != nil {
		log.Fatalln("could not open "+*originalPath, err)
	}
	defer handle.Close()
	originalBytes, err := ioutil.ReadAll(handle)
	var originalRecord map[string]interface{}
	json.Unmarshal(originalBytes, &originalRecord)

	handle, err = os.Open(*updatePath)
	if err != nil {
		log.Fatalln("could not open "+*updatePath, err)
	}
	defer handle.Close()
	updateBytes, err := ioutil.ReadAll(handle)
	var update map[string]interface{}
	json.Unmarshal(updateBytes, &update)

	updatedRecord := utils.UpdateMap(originalRecord, update)

	printMap(originalRecord)
	printMap(update)
	printMap(updatedRecord)

}

func printMap(m map[string]interface{}) {
	b, _ := json.Marshal(m)
	fmt.Println(string(b))
}
