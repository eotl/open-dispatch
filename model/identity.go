package model

import (
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/model"
	"encoding/json"
	"github.com/google/uuid"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

const LEVEL_VERIFIED = "verified"
const LEVEL_ADMIN = "admin"
const LEVEL_UNVERIFIED = "unverified"

const GROUP_ROLE_ORGANIZER = "organizer"
const GROUP_ROLE_MEMBER = "member"

type IdentityKey struct {
	ID         string                 `json:"identity" gorm:"primaryKey"`
	Level      string                 `json:"level"`
	Status     string                 `json:"status"`
	Info       map[string]interface{} `json:"info" gorm:"-"`
	InfoDB     datatypes.JSON         `json:"-" gorm:"column:info"`
	GroupRoles []GroupMember          `json:"-"`
}

func (identityKey IdentityKey) Id() string {
	return identityKey.ID
}

func (identityKey IdentityKey) Roles() []string {
	return []string{identityKey.Level}
}

func (identityKey IdentityKey) IsVerified() bool {
	return identityKey.Level == LEVEL_VERIFIED || identityKey.Level == LEVEL_ADMIN
}

func (identityKey *IdentityKey) BeforeSave(db *gorm.DB) (err error) {
	if identityKey.Info != nil {
		jsonBytes, err := json.Marshal(identityKey.Info)
		if err == nil {
			identityKey.InfoDB = jsonBytes
		}
	}
	return
}

func (identityKey *IdentityKey) AfterFind(db *gorm.DB) (err error) {
	if identityKey.InfoDB != nil {
		jsonMap := map[string]interface{}{}
		err = json.Unmarshal(identityKey.InfoDB, &jsonMap)
		if err == nil {
			identityKey.Info = jsonMap
		}
	}
	return
}

type Invite struct {
	model.Base
	InviteCode    string `json:"invite_code" gorm:"unique"`
	IdentityKeyID string `json:"invite_key_id"`
}

func (invite Invite) Id() string {
	return invite.InviteCode
}

type Group struct {
	model.Base
	Name         string        `json:"name"`
	Description  string        `json:"description"`
	GroupMembers []GroupMember `json:"members"`
	ShortCodes   []ShortCode   `json:"short_codes"`
}

type GroupMember struct {
	model.Base
	GroupID       uuid.UUID   `json:"-" gorm:"uniqueIndex:idx_group_member"`
	Group         Group       `json:"-" openapi:"-"`
	IdentityKeyID string      `json:"-" gorm:"uniqueIndex:idx_group_member"`
	IdentityKey   IdentityKey `json:"identity_key" openapi:"-"`
	Role          string      `json:"role"`
}

type ShortCode struct {
	ID          string       `json:"id" gorm:"primarykey"`
	GroupID     uuid.UUID    `json:"-"`
	Group       Group        `json:"-" openapi:"-"`
	Assignments []Assignment `json:"assignments"`
}

func (shortCode ShortCode) IsTaken() bool {
	for _, assignment := range shortCode.Assignments {
		if !assignment.DeletedAt.Valid {
			return true
		}
	}
	return false
}
