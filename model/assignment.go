package model

import "codeberg.org/eotl/open-dispatch-daemon/iris-helpers/model"

type Assignment struct {
	model.Base
	ShipmentID  string `json:"shipment_id" gorm:"<-:create"`
	ShortCodeID string `json:"short_code" gorm:"<-:create"`
}
