import Vue from 'vue'
import VueRouter from 'vue-router'

import { AccountView, InvitesView } from '@eotl/core/views'

Vue.use(VueRouter)

const DEFAULT_TITLE = 'Open Dispatch'

// route level code-splitting generate separate chunk (about.[hash].js) for
// route which is lazy-loaded when the route is visited.
// webpackChunkName: "dispatch"
const routes = [{
    path: '/',
    name: 'landing',
    component: () => import('@/views/Landing.vue'),
    meta: {
      title: 'Open Dispatch',
      metaTags: [{
        name: 'description',
        content: 'Welcome to this instance of Open Dispatch.'
      },{
        property: 'og:description',
        content: 'Welcome to this instance of Open Dispatch.'
      }]
    }
  },{
    path: '/home',
    name: 'home',
    component: () => import('@/views/Home.vue'),
    meta: {
      title: 'Home',
    }
  },{
    path: '/new',
    name: 'new',
    component: () => import('@/views/Estimator.vue'),
    meta: {
      title: 'New Shipment',
    }
  },{
    path: '/about',
    name: 'about',
    component: () => import('@/views/About.vue'),
    meta: {
      title: 'About',
    }
  },{
    path: '/account',
    name: 'account',
    component: () => import(AccountView),
    meta: {
      title: 'Account',
    }
  },{
    path: '/groups',
    name: 'groups',
    component: () => import('@/views/Groups.vue'),
    meta: {
      title: 'Groups',
    }
  },{
    path: '/invites',
    name: 'invites',
    component: () => import(InvitesView),
    meta: {
      title: 'Invites',
    }
  },{
    path: '/shipments',
    name: 'shipments',
    component: () => import('@/views/Shipments.vue'),
    meta: {
      title: 'Shipments',
    }
  },{
    path: '/shipment/:id',
    name: 'shipment',
    component: () => import('@/views/Shipment.vue'),
    meta: {
      title: 'View Shipment',
    }
  },{
    path: '/support',
    name: 'support',
    component: () => import('@/views/Support.vue'),
    meta: {
      title: 'Support',
    }
  },{
    path: '/users',
    name: 'users',
    component: () => import('@/views/Users.vue'),
    meta: {
      title: 'Users',
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.afterEach((to) => {
  // Page history with (to, from) =>
  Vue.nextTick(() => {
    document.title = to.meta.title || DEFAULT_TITLE
  })
})

export default router
