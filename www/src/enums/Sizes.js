const sizes = {
  sm: {
    width: '10 cm',
    length: '25 cm',
    height: '12 cm'
  },
  md: {
    width: '25 cm',
    length: '50 cm',
    height: '12 cm'
  },
  lg: {
    width: '25 cm',
    length: '50 cm',
    height: '20 cm'
  },
  xl: {
    width: '50 cm',
    length: '100 cm',
    height: '20 cm'
  }
}

export default sizes;
