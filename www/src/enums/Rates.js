const rates = {
  size: {
    sm: 1.5,
    md: 2,
    lg: 2.5,
    xl: 3.5
  },
  weight: {
    light: 1,
    average: 1.25,
    heavy: 1.75,
    very: 2,
    crazy: 3
  },
  speed: {
    immediate: 2.5,
    day: 2,
    next: 1.75,
    second: 1.5,
    week: 1.25
  },
  distance: {
    average: 1,
  },
}

export default rates;
