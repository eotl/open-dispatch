const prefix = 'eotl_';

export function readStorage(key, initialize) {
    key = prefix + key;
    let value = localStorage.getItem(key);
    let parsedValue;
    if (value !== null) {
        try {
            parsedValue = JSON.parse(value);
        } catch (_) {}
    }
    if (parsedValue === undefined) {
        parsedValue = initialize();
        localStorage.setItem(key, JSON.stringify(parsedValue));
    }
    return parsedValue;
}

export function writeStorage(key, value = null) {
    key = prefix + key;
    localStorage.setItem(key, JSON.stringify(value));
}

export function persistKeys(store, keys) {
    for (let key of keys) {
        store.watch(
            state => state[key],
            value => {
                writeStorage(key, value);
            }
        );
    }
}
