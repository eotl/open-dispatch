import Vue from 'vue';
import Vuex from 'vuex';
import { ssr, ssrIdentity } from '@eotl/simple-signed-records';

import { persistKeys, readStorage } from './localstorage';
import config from '../default_config';

Vue.use(Vuex);

const development = process.env.NODE_ENV === 'development';

let accountDefault = {
    status: 'new',
    cert: '',
    type: 'unspecified',
    lang: 'en'
};

const store = new Vuex.Store({
  state: {
    config,
    configLoaded: false,
    daemon: {},
    alert: {
      show: false,
      style: 'primary',
      title: 'Hello',
      message: 'This is just a default status message. Nothing to see here',
    },
    debug: false,
    account: readStorage('account', () => accountDefault),
    identity: readStorage('identity', () => ssr.createIdentity()),
    development
  },
  mutations: {
    merge(state, newState) {
      for (let key in newState) {
        if (key in state) {
          state[key] = newState[key];
        }
      }
    }
  },
  actions: {
    loadConfig({ commit, state }) {
      fetch('/config.json')
        .then(response => response.json())
        .then(data => {
          commit('merge', {
            config: { ...state.config, ...data },
            configLoaded: true
          });
        })
        .catch(() => {});
    },
    loadDaemon({ commit, state }) {
      fetch(state.config.api + '/info')
        .then(response => response.json())
        .then(data => {
          commit('merge', {
            daemon: {
              name: data.name,
              description: data.description,
              status: data.status,
              version: data.version,
              url: data.url
            }
          });
        })
        .catch(() => {});
    },
    alertMsg({ commit }, alert) {
      commit('merge', { alert })
    },
    restoreAccount({ commit }, account) {
      commit('merge', { account })
    },
    restoreIdentity({ commit }, identity) {
      commit('merge', { identity })
    },
  }
});

// persist some store keys to localStorage
persistKeys(store, ['identity', 'account']);

// for debugging
if (development) window.store = store;

export default store;
