const messages = {
    start:{
      title: 'Start',
      welcome: 'Welcome',
      welcomeText: 'Open Dispatch is a simple way to coordinate shipping things from one place to another, ideally by bicycle or wind.',
    }
}

export default messages;
