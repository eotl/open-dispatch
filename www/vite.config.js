import { fileURLToPath, URL } from 'node:url'
import path, { resolve } from 'path'

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import { viteStaticCopy } from 'vite-plugin-static-copy'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        viteStaticCopy({
            targets: [
                { src: path.resolve(__dirname, "node_modules/leaflet/dist/images/*"), dest: './public/images' },
                { src: path.resolve(__dirname, "node_modules/@eotl/icons/dist/*"), dest: './public/images/icons' },
                { src: path.resolve(__dirname, "node_modules/@eotl/theme-bootstrap/dist/fonts/*"), dest: './public/fonts' },
                { src: path.resolve(__dirname, "node_modules/@eotl/theme-bootstrap/dist/js/*"), dest: './public/js' },
            ],
        }),
        vue()
    ],
    resolve: {
        alias: [
            {
                find: '@',
                replacement: resolve(__dirname, 'src')
            }
        ],
        extensions: ['.js', '.vue']
    },
    build: {
        outDir: 'dist',
        emptyOutDir: true,
    },
    server: {
        port: 8086,
        host: '0.0.0.0',
        build: {
            outDir: 'dist/',
            sourcemap: true,
            chunkSizeWarningLimit: 25000,
            rollupOptions: {
                output: {
                    manualChunks: {
                        'libs': ['vue'],
                    },
                },
            },
        },
    }
})
