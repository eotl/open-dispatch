// listener.go - open-dispatch http listener.
// Copyright (C) 2020 Masala.
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package listener

import (
	"codeberg.org/eotl/open-dispatch-daemon/config"
	"codeberg.org/eotl/open-dispatch-daemon/controllers/groups"
	"codeberg.org/eotl/open-dispatch-daemon/controllers/invites"
	"codeberg.org/eotl/open-dispatch-daemon/controllers/keys"
	"codeberg.org/eotl/open-dispatch-daemon/controllers/shipments"
	"codeberg.org/eotl/open-dispatch-daemon/files"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/acl"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/controllers"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/openapi"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/ssr"
	"codeberg.org/eotl/open-dispatch-daemon/model"
	"codeberg.org/eotl/open-dispatch-daemon/utils"
	"errors"
	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
	"github.com/kataras/iris/v12/core/router"
	"github.com/kataras/iris/v12/middleware/accesslog"
	"github.com/thoas/go-funk"
	"gorm.io/gorm"
	"os"
	"reflect"
	"sort"
)

// New returns a new HttpService
func New(db *gorm.DB, configuration config.Config) (*iris.Application, error) {

	err := db.AutoMigrate(
		&model.Shipment{},
		&model.ShipmentRecord{},
		&model.IdentityKey{},
		&model.Invite{},
		&model.Group{},
		&model.GroupMember{},
		&model.ShortCode{},
		&model.Assignment{},
	)
	if err != nil {
		return nil, err
	}

	app := iris.New()

	app.UseRouter(cors.AllowAll(), accesslog.New(os.Stdout).Handler)

	_acl := buildAcl()
	api := openapi.API{
		PathPrefix: "/api/v1",
		Controllers: map[string]controllers.RESTController{
			"keys":      keys.New(app, db, _acl),
			"shipments": shipments.New(app, db, _acl),
			"invites":   invites.New(app, db, _acl),
			"groups":    groups.New(app, db, _acl),
		},
		Middlewares: []context.Handler{ssr.RequestVerification(createIdentityProvider(db), "/api/v1/spec")},
	}

	api.Mount(app)

	filesRoute := app.Party("/files", ssr.RequestVerification(createIdentityProvider(db)), files.FileAuthorizer())
	{
		filesRoute.HandleDir("/", iris.Dir(configuration.StaticFilesPath))
	}

	app.Get("/info", func(context *context.Context) {
		serverInfo := map[string]interface{}{
			"api":         routeDescriptions(app),
			"status":      getServerStatus(db),
			"name":        configuration.Instance,
			"version":     "v1",
			"description": configuration.Description,
			"url":         configuration.Url,
		}
		_, _ = context.JSON(serverInfo)
		context.Next()
	})

	return app, nil
}

func getServerStatus(db *gorm.DB) string {
	if hasAdmin(db) {
		return "configured"
	} else {
		return "unconfigured"
	}
}

func hasAdmin(db *gorm.DB) bool {
	var count int64
	result := db.Find(&model.IdentityKey{}).Count(&count)
	if result.Error != nil {
		panic(result.Error)
	}
	return count > 0
}

func routeDescriptions(app *iris.Application) []map[string]string {
	routeDescriptions, _ := funk.Map(app.GetRoutes(), func(route *router.Route) map[string]string {
		return map[string]string{
			"method": route.Method,
			"path":   route.Path,
		}
	}).([]map[string]string)
	sort.Slice(routeDescriptions, func(p, q int) bool {
		if routeDescriptions[p]["path"] == routeDescriptions[q]["path"] {
			return routeDescriptions[p]["method"] < routeDescriptions[q]["method"]
		}
		return routeDescriptions[p]["path"] < routeDescriptions[q]["path"]
	})
	return routeDescriptions
}

func createIdentityProvider(db *gorm.DB) ssr.IdentityProvider {
	return func(id string) (ssr.Identity, error) {
		identityKey := model.IdentityKey{}
		result := db.First(&identityKey, "id = ?", id)
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			identityKey.ID = id

			if hasAdmin(db) {
				identityKey.Level = model.LEVEL_UNVERIFIED
			} else {
				identityKey.Level = model.LEVEL_ADMIN
			}
			db.Create(&identityKey)

		}
		return &identityKey, nil
	}
}

func buildAcl() *acl.ACL {
	dispatchAcl := acl.New()

	// Admin may do anything
	dispatchAcl.AllowByPattern([]string{model.LEVEL_ADMIN}, nil, nil, nil)

	// Verified Users may create shipments
	dispatchAcl.AllowByPattern([]string{model.LEVEL_VERIFIED}, nil, []reflect.Type{model.ShipmentType}, []string{"create"})

	// Owner, supplier, deliverers can update shipment
	dispatchAcl.AllowByFunction(func(identity ssr.Identity, _type *reflect.Type, object interface{}, action string) bool {
		if shipment, ok := object.(model.Shipment); ok {
			if shipment.Identity == identity.Id() {
				return true
			}
			data, err := shipment.ConsolidateShipmentData()

			if err != nil {
				return false
			}
			if roles, ok := data["roles"].(map[string]interface{}); ok {

				if suppliers, ok := roles["supplier"].([]interface{}); ok {
					if utils.IsValueInList(identity.Id(), suppliers) {
						return true
					}
				}
				if delivery, ok := roles["delivery"].([]interface{}); ok {
					if utils.IsValueInList(identity.Id(), delivery) {
						return true
					}
				}
			}

		}
		return false
	})

	// Organizer can add member to group
	dispatchAcl.AllowByFunction(func(identity ssr.Identity, _type *reflect.Type, object interface{}, action string) bool {
		if newMember, ok := object.(*model.GroupMember); ok {
			group := newMember.Group
			for _, member := range group.GroupMembers {
				if member.IdentityKey.ID == identity.Id() && member.Role == model.GROUP_ROLE_ORGANIZER {
					return true
				}
			}
		}
		return false
	})

	return dispatchAcl
}
