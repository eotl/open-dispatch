package shipments

import (
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/acl"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/controllers"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/ssr"
	"codeberg.org/eotl/open-dispatch-daemon/model"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/imdario/mergo"
	"github.com/kataras/iris/v12"
	"gorm.io/datatypes"
	"gorm.io/gorm"
	"net/http"
	"reflect"
)

func ShipmentFromRequest(ctx iris.Context, db *gorm.DB) (controllers.ControllerObject, error) {

	shipment := new(model.Shipment)

	err := json.Unmarshal(ssr.GetCertifiedBody(ctx), &shipment.RawShipment)
	if err != nil {
		return nil, err
	}

	shipment.SignData = ssr.GetSignData(ctx)
	return shipment, nil
}

func AssignmentFromRequest(ctx iris.Context) (*model.Assignment, error) {

	assignment := new(model.Assignment)

	err := json.Unmarshal(ssr.GetCertifiedBody(ctx), &assignment)
	if err != nil {
		return nil, err
	}

	return assignment, nil
}

func RecordFromRequest(ctx iris.Context, controller controllers.RESTController) (*model.ShipmentRecord, error) {

	clearRecord := map[string]interface{}{}
	err := json.Unmarshal(ssr.GetCertifiedBody(ctx), &clearRecord)
	if err != nil {
		return nil, err
	}
	record := model.ShipmentRecord{
		ID:         ssr.GetSignData(ctx).ID,
		ShipmentID: controller.GetIdString(ctx),
		Data:       datatypes.JSON(ssr.GetOriginalRecord(ctx)),
	}

	return &record, nil
}

func New(app *iris.Application, db *gorm.DB, acl *acl.ACL) controllers.RESTController {
	var controller controllers.RESTController
	controller = controllers.RESTController{
		App:                 app,
		DB:                  db,
		ACL:                 acl,
		ControllerType:      &model.ShipmentType,
		ParseBody:           ShipmentFromRequest,
		IdConverter:         controllers.StringConverter,
		CustomCreateHandler: create(app, &controller, db),
		Scope: func(ctx iris.Context) *gorm.DB {
			return db.Preload("Assignment")
		},
		CustomReadHandler: func(ctx iris.Context) {
			object := &model.Shipment{}
			result := db.Preload("Assignment").Preload("Records", func(db *gorm.DB) *gorm.DB {
				return db.Order("created_at")
			}).First(object, "id = ?", controller.GetId(ctx))
			identity := ssr.GetIdentity(ctx)
			if controller.CanRead(identity, object) {
				data, err := object.ConsolidateShipmentData()
				if err != nil {
					iris_helpers.HandleInternalError(ctx, app, result.Error)
					return
				}
				controllers.EncodeAndReturn(ctx, result, data)
			} else {
				controller.Veto(ctx, identity, "create")
			}
			return
		},
		AdditionalInstanceHandlers: map[string]controllers.AdditionalHandlers{
			"assign-short-code": {
				MethodHandlers: map[string]controllers.AdditionalHandler{
					"post": {
						Handler: func(ctx iris.Context) {

							shipment := model.Shipment{}
							result := db.Preload("Assignment").Preload("Records", func(db *gorm.DB) *gorm.DB {
								return db.Order("created_at ")
							}).First(&shipment, "id = ?", controller.GetId(ctx))

							if result.Error != nil {
								iris_helpers.HandleInternalError(ctx, app, result.Error)
								return
							}
							if !controller.CanUpdate(ssr.GetIdentity(ctx), shipment) {
								iris_helpers.AbortWithError(ctx, http.StatusForbidden, fmt.Errorf("key is not allowed to update this shipment"))
								return
							}

							if shipment.Assignment.ID.Variant() == uuid.RFC4122 {
								iris_helpers.AbortWithError(ctx, http.StatusConflict, fmt.Errorf("Shipment already assigned"))
								return
							}

							assignment, err := AssignmentFromRequest(ctx)
							if err != nil {
								iris_helpers.AbortWithError(ctx, http.StatusBadRequest, fmt.Errorf("Failed to construct Assignment from request: %v\n", err))
								return
							}

							shortCode := model.ShortCode{}
							result = db.Preload("Assignments").First(&shortCode, "id = ?", assignment.ShortCodeID)
							if result.Error != nil {
								iris_helpers.HandleInternalError(ctx, app, result.Error)
								return
							}

							if shortCode.IsTaken() {
								iris_helpers.AbortWithError(ctx, http.StatusConflict, fmt.Errorf("ShortCode taken"))
								return
							}

							assignment.ShipmentID = shipment.ID

							result = controller.DB.Create(assignment)
							controllers.EncodeAndReturn(ctx, result, assignment)
						},
						InputType:  reflect.TypeOf(model.Assignment{}),
						OutputType: reflect.TypeOf(model.Assignment{}),
					},
				},
			},
			"records": {
				MethodHandlers: map[string]controllers.AdditionalHandler{
					"get": {
						Handler: func(ctx iris.Context) {
							object := model.Shipment{}
							result := db.Preload("Records").First(&object, "id = ?", controller.GetId(ctx))

							identity := ssr.GetIdentity(ctx)
							if controller.CanRead(identity, object) {
								controllers.EncodeAndReturn(ctx, result, object.Records)
							} else {
								controller.Veto(ctx, identity, "read")
							}
							return
						},
						OutputType: reflect.TypeOf([]model.ShipmentRecord{}),
					},
				},
			},
			"updates": {
				MethodHandlers: map[string]controllers.AdditionalHandler{
					"post": {
						Handler: func(ctx iris.Context) {
							record, err := RecordFromRequest(ctx, controller)
							if err != nil {
								iris_helpers.AbortWithError(ctx, http.StatusBadRequest, fmt.Errorf("Failed to construct ShipmentRecord from request: %v\n", err))
								return
							}

							object := &model.Shipment{}
							result := db.Preload("Assignment").Preload("Records", func(db *gorm.DB) *gorm.DB {
								return db.Order("created_at")
							}).First(object, "id = ?", controller.GetId(ctx))

							if result.Error != nil {
								iris_helpers.HandleInternalError(ctx, app, result.Error)
								return
							}

							if !controller.CanUpdate(ssr.GetIdentity(ctx), *object) {
								iris_helpers.AbortWithError(ctx, http.StatusForbidden, fmt.Errorf("key is not allowed to update this shipment"))
								return
							}
							updateData := map[string]interface{}{}

							if err = json.Unmarshal(record.Data, &updateData); err != nil {
								iris_helpers.HandleInternalError(ctx, app, err)
								return
							}

							if err := mergo.Map(object, updateData, mergo.WithOverride); err != nil {
								iris_helpers.HandleInternalError(ctx, app, err)
								return
							}

							object.Records = append(object.Records, *record)

							result = db.Save(object)
							if result.Error != nil {
								iris_helpers.HandleInternalError(ctx, app, result.Error)
								return
							} else {
								data, _ := object.ConsolidateShipmentData()
								controllers.EncodeAndReturn(ctx, result, data)
							}
						},
						InputType:  reflect.TypeOf(model.ShipmentRecord{}),
						OutputType: model.ShipmentType,
					},
				},
			},
		},
	}
	return controller
}

func create(app *iris.Application, controller *controllers.RESTController, db *gorm.DB) func(ctx iris.Context) {
	return func(ctx iris.Context) {

		object, err := ShipmentFromRequest(ctx, controller.DB)
		if err != nil {
			iris_helpers.AbortWithError(ctx, http.StatusBadRequest, fmt.Errorf("Failed to construct %v from request: %v\n", model.ShipmentType, err))
			return
		}

		identity := ssr.GetIdentity(ctx)

		if controller.CanCreate(identity, object) {
			result := db.Create(object)
			if result.Error != nil {
				iris_helpers.HandleInternalError(ctx, app, result.Error)
				return
			} else {
				record := model.ShipmentRecord{
					ID:         object.Id(),
					ShipmentID: object.Id(),
					Data:       datatypes.JSON(ssr.GetOriginalRecord(ctx)),
				}
				result = db.Create(&record)
				if result.Error != nil {
					iris_helpers.HandleInternalError(ctx, app, result.Error)
					return
				}
				ctx.StatusCode(http.StatusCreated)
				controllers.EncodeAndReturn(ctx, result, object)
			}
		} else {
			controller.Veto(ctx, identity, "create")
			return
		}
	}
}
