package keys

import (
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/acl"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/controllers"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/ssr"
	"codeberg.org/eotl/open-dispatch-daemon/model"
	"encoding/json"
	"github.com/kataras/iris/v12"
	"gorm.io/gorm"
	"reflect"
)

func IdentityFromRequest(ctx iris.Context, db *gorm.DB) (controllers.ControllerObject, error) {

	identity := new(model.IdentityKey)

	err := json.Unmarshal(ssr.GetCertifiedBody(ctx), &identity)
	if err != nil {
		return nil, err
	}

	return identity, nil
}

func New(app *iris.Application, db *gorm.DB, acl *acl.ACL) controllers.RESTController {

	controllerType := reflect.TypeOf(model.IdentityKey{})
	controller := controllers.RESTController{
		App:            app,
		DB:             db,
		ACL:            acl,
		ControllerType: &controllerType,
		ParseBody:      IdentityFromRequest,
		IdConverter:    controllers.StringConverter,
	}
	return controller
}
