package groups

import (
	iris_helpers "codeberg.org/eotl/open-dispatch-daemon/iris-helpers"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/acl"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/controllers"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/ssr"
	"codeberg.org/eotl/open-dispatch-daemon/model"
	"codeberg.org/eotl/open-dispatch-daemon/utils"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/kataras/iris/v12"
	"gorm.io/gorm"
	"net/http"
	"reflect"
)

func GroupFromRequest(ctx iris.Context, db *gorm.DB) (controllers.ControllerObject, error) {

	group := new(model.Group)

	err := json.Unmarshal(ssr.GetCertifiedBody(ctx), &group)
	if err != nil {
		ctx.Application().Logger().Error(string(ssr.GetCertifiedBody(ctx)))
		return nil, err
	}

	return group, nil
}

func MemberFromRequest(ctx iris.Context, db *gorm.DB) (controllers.ControllerObject, error) {

	member := new(model.GroupMember)

	groupId := uuid.MustParse(controllers.GetIdString(ctx, reflect.TypeOf(model.Group{})))

	err := json.Unmarshal(ssr.GetCertifiedBody(ctx), &member)
	if err != nil {
		ctx.Application().Logger().Error(string(ssr.GetCertifiedBody(ctx)))
		return nil, err
	}

	member.GroupID = groupId

	result := db.Find(&member.Group, groupId)
	if result.Error != nil {
		panic(result.Error)
	}

	return member, nil
}

func New(app *iris.Application, db *gorm.DB, acl *acl.ACL) controllers.RESTController {
	groupType := reflect.TypeOf(model.Group{})
	memberType := reflect.TypeOf(model.GroupMember{})
	var controller controllers.RESTController
	controller = controllers.RESTController{
		App:            app,
		DB:             db,
		ACL:            acl,
		ControllerType: &groupType,
		ParseBody:      GroupFromRequest,
		Scope: func(ctx iris.Context) *gorm.DB {
			return db.Preload("GroupMembers.IdentityKey").Preload("ShortCodes.Assignments")
		},
		SubControllers: map[string]controllers.RESTController{
			"members": {
				App:            app,
				DB:             db,
				ACL:            acl,
				ControllerType: &memberType,
				ParseBody:      MemberFromRequest,
				Scope: func(ctx iris.Context) *gorm.DB {
					object := &model.Group{}
					_ = db.First(object, "id = ?", controller.GetId(ctx))
					return db.Preload("IdentityKey").Where(object, object)
				},
			},
		},
		CustomCreateHandler: func(ctx iris.Context) {
			object, err := controller.ParseBody(ctx, controller.DB)
			if err != nil {
				iris_helpers.AbortWithError(ctx, http.StatusBadRequest, fmt.Errorf("Failed to construct %v from request: %v\n", controller.ControllerType, err))
				return
			}

			identity := ssr.GetIdentity(ctx)

			if !controller.CanCreate(identity, object) {
				controller.Veto(ctx, identity, "create")
				return
			}

			group := object.(*model.Group)

			result := controller.DB.Create(group)

			ctx.StatusCode(http.StatusCreated)
			controllers.EncodeAndReturn(ctx, result, group)
		},
		AdditionalInstanceHandlers: map[string]controllers.AdditionalHandlers{
			"short-codes": {
				MethodHandlers: map[string]controllers.AdditionalHandler{
					"post": {
						Handler: func(ctx iris.Context) {
							group := model.Group{}
							result := db.Preload("GroupMembers").First(&group, "id = ?", controller.GetId(ctx))
							if result.Error != nil {
								iris_helpers.HandleInternalError(ctx, app, result.Error)
								return
							}

							if !controller.CanUpdate(ssr.GetIdentity(ctx), group) {
								iris_helpers.AbortWithError(ctx, http.StatusForbidden, fmt.Errorf("key is not allowed to update this group"))
								return
							}

							shortCode := &model.ShortCode{
								ID:      utils.ShortCode(),
								GroupID: group.ID,
							}

							result = controller.DB.Create(shortCode)
							ctx.StatusCode(http.StatusCreated)
							controllers.EncodeAndReturn(ctx, result, shortCode)

						},
						InputType:  nil,
						OutputType: reflect.TypeOf(model.ShortCode{}),
					},
					"get": {
						Handler: func(ctx iris.Context) {
							group := model.Group{}
							result := db.Preload("ShortCodes.Assignments").First(&group, "id = ?", controller.GetId(ctx))
							if result.Error != nil {
								iris_helpers.HandleInternalError(ctx, app, result.Error)
								return
							}
							if ctx.URLParamDefault("free", "true") == "free" {
								freeShortCodes := []model.ShortCode{}
								for _, shortCode := range group.ShortCodes {
									if !shortCode.IsTaken() {
										freeShortCodes = append(freeShortCodes, shortCode)
									}
								}

								controllers.EncodeAndReturn(ctx, result, freeShortCodes)
							} else {
								controllers.EncodeAndReturn(ctx, result, group.ShortCodes)
							}

						},
						OutputType: reflect.SliceOf(reflect.TypeOf(model.ShortCode{})),
					},
				},
			},
		},
	}
	return controller
}
