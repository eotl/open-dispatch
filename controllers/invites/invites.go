package invites

import (
	iris_helpers "codeberg.org/eotl/open-dispatch-daemon/iris-helpers"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/acl"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/controllers"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/ssr"
	"codeberg.org/eotl/open-dispatch-daemon/model"
	"encoding/json"
	"fmt"
	"github.com/kataras/iris/v12"
	"gorm.io/gorm"
	"net/http"
	"reflect"
)

func InviteFromRequest(ctx iris.Context, db *gorm.DB) (controllers.ControllerObject, error) {

	invite := new(model.Invite)

	err := json.Unmarshal(ssr.GetCertifiedBody(ctx), &invite)
	if err != nil {
		ctx.Application().Logger().Error(string(ssr.GetCertifiedBody(ctx)))
		return nil, err
	}

	return invite, nil
}

func New(app *iris.Application, db *gorm.DB, acl *acl.ACL) controllers.RESTController {

	controllerType := reflect.TypeOf(model.Invite{})
	controller := controllers.RESTController{
		App:            app,
		DB:             db,
		ACL:            acl,
		ControllerType: &controllerType,
		ParseBody:      InviteFromRequest,
		AdditionalHandlers: map[string]controllers.AdditionalHandlers{
			"/use": {
				MethodHandlers: map[string]controllers.AdditionalHandler{
					"post": {
						Handler: func(ctx iris.Context) {
							inviteFromRequest, err := InviteFromRequest(ctx, db)
							if err != nil {
								iris_helpers.AbortWithError(ctx, http.StatusBadRequest, fmt.Errorf("Failed to construct Invite from request: %v\n", err))
								return
							}
							if invite, ok := inviteFromRequest.(*model.Invite); ok {
								if invite.IdentityKeyID != "" {
									iris_helpers.AbortWithError(ctx, http.StatusConflict, fmt.Errorf("Invite token %v already used\n", invite.InviteCode))
									return
								}
								result := db.First(invite, invite)
								if result.Error != nil {
									iris_helpers.AbortWithError(ctx, http.StatusNotFound, fmt.Errorf("Failed to find Invite from request: %v\n", err))
									return
								}
								if identityKey, ok := (ssr.GetIdentity(ctx)).(*model.IdentityKey); ok {
									if identityKey.IsVerified() {
										iris_helpers.AbortWithError(ctx, http.StatusConflict, fmt.Errorf("User already verified: %v\n", err))
										return
									} else {
										invite.IdentityKeyID = identityKey.ID
										identityKey.Level = model.LEVEL_VERIFIED
										result = db.Save(&identityKey)
										result = db.Save(invite)
										controllers.EncodeAndReturn(ctx, result, invite)
										return
									}
								} else {
									iris_helpers.AbortWithError(ctx, http.StatusBadRequest, fmt.Errorf("Failed to assert IdentityKey from context"))
									return
								}
							} else {
								iris_helpers.AbortWithError(ctx, http.StatusBadRequest, fmt.Errorf("Failed to assert Invite from request"))
								return
							}

						},
						InputType:  controllerType,
						OutputType: controllerType,
					},
				},
			},
		},
	}
	return controller
}
