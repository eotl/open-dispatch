FROM golang:alpine AS builder

COPY . ./open-dispatch-daemon
RUN cd open-dispatch-daemon/cmd/server && go build

FROM alpine AS runner
COPY --from=builder /go/open-dispatch-daemon/cmd/server/server .
EXPOSE 8000
CMD /server

