package files

import (
	irishelpers "codeberg.org/eotl/open-dispatch-daemon/iris-helpers"
	"codeberg.org/eotl/open-dispatch-daemon/iris-helpers/ssr"
	"codeberg.org/eotl/open-dispatch-daemon/model"
	"github.com/kataras/iris/v12/context"
	"github.com/pkg/errors"
	"net/http"
)

func FileAuthorizer() context.Handler {
	return func(ctx *context.Context) {
		identity, valid := ssr.GetIdentity(ctx).(*model.IdentityKey)
		if valid && identity.IsVerified() {
			ctx.Next()
		} else {
			irishelpers.AbortWithError(ctx, http.StatusForbidden, errors.Errorf("You need to be verified to access this file"))
		}
	}
}
