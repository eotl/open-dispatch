module codeberg.org/eotl/open-dispatch-daemon

go 1.14

require (
	github.com/Joker/hpp v1.0.0 // indirect
	github.com/btcsuite/btcutil v1.0.2
	github.com/caarlos0/env/v6 v6.4.0
	github.com/flosch/pongo2/v4 v4.0.2 // indirect
	github.com/go-openapi/spec v0.19.14
	github.com/google/uuid v1.1.2
	github.com/imdario/mergo v0.3.11
	github.com/iris-contrib/middleware/cors v0.0.0-20201012144513-9d3d1026d313
	github.com/jordan-wright/email v4.0.1-0.20200917010138-e1c00e156980+incompatible
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/kataras/iris/v12 v12.2.0-alpha.0.20201012125253-8e51a296b968
	github.com/matoous/go-nanoid/v2 v2.0.0
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mitchellh/copystructure v1.0.0
	github.com/pkg/errors v0.8.1
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/thoas/go-funk v0.7.0
	github.com/yudai/pp v2.0.1+incompatible // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/yaml.v2 v2.3.0
	gorm.io/datatypes v1.0.0
	gorm.io/driver/postgres v1.0.5
	gorm.io/gorm v1.20.5
)
